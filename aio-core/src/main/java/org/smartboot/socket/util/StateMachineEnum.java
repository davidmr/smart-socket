package org.smartboot.socket.util;

/**
 * @author 三刀
 * @version V1.0 , 2017/9/12
 */
public enum StateMachineEnum {
    NEW_SESSION, INPUT_SHUTDOWN, INPUT_EXCEPTION, OUTPUT_EXCEPTION;
}
